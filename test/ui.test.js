const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {

})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {

})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {

})

// 6
test('[Content] Welcome message', async () => {

})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {

})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {

})

// 11
test('[Content] The "Send" button should exist', async () => {

})

// 12
test('[Behavior] Send a message', async () => {

})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    
})

// 14
test('[Content] The "Send location" button should exist', async () => {

})

// 15
test('[Behavior] Send a location message', async () => {

})
